# YouGui Script - README #
---

### Overview ###

The **YouGui** tool is a simple batch script and a GUI for the well known [**youtube-dl**](http://ytdl-org.github.io/youtube-dl/) command line tool. The user will be asked for some parameters and the script will download the specified video from the Youtube website. The user can specify the video code from the Youtube URL of the desired video and choose the video/audio format to be downloaded. More information about the **youtube-dl** tool can be found here:

[**Website**](http://ytdl-org.github.io/youtube-dl/)
[**GitHub Repository**](https://github.com/ytdl-org/youtube-dl/) 
[**Latest Windows Executable**](https://yt-dl.org/latest/youtube-dl.exe)

### Screenshots ###

![YouGui - Specify video code](development/readme/yougui1.png "YouGui - Specify video code")

![YouGui - Specify video/audio format](development/readme/yougui2.png "YouGui - Specify video/audio format")

![YouGui - Download in progress](development/readme/yougui3.png "YouGui - Download in progress")

![YouGui - Download finished](development/readme/yougui4.png "YouGui - Download finished")

### Setup ###

* Copy the directory **yougui** to your Windows computer.
* The directory contains the **youtube-dl.exe** executable, the **yougui.bat** batch file and the license folder.
* Doubleclick the script file **yougui.bat** to start a new download.
* On the first screen you have to enter the video code (example: **p5KgjeZB8Ww**) from the Youtube video URL.
* You can copy the code from the URL and paste it here (paste text with click on the right mouse button).
* You can also press **x** to exit the script without downloading anything.
* Then press **Enter** and the available video/audio formats will be displayed.
* Enter a **number** from the list or enter **b** if you want to download the best video quality.
* You can also press **x** to exit the script without downloading anything.
* After choosing a format press **Enter** again and just wait until the video is downloaded.
* If you press any key the script will start again.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **YouGui** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
