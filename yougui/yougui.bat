@ECHO off

REM ####################################################################
REM #                                                                  #
REM #                           Y O U G U I                            #
REM #                                                                  #
REM #           Youtube Video Downloader GUI for 'youtube-dl'          #
REM #                                                                  #
REM #            Copyright 2019 by PB-Soft / Patrick Biegel            #
REM #                                                                  #
REM #                       https://pb-soft.com                        #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #                 YOUGUI version: 1.0 / 06.06.2019                 #
REM #                                                                  #
REM #                 Youtube-dl version: 2019.5.20.0                  #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #   YOUGUI Repository: https://bitbucket.org/Blackneron/YouGui/    #
REM #                                                                  #
REM #   Youtube-dl Website: https://ytdl-org.github.io/youtube-dl/     #
REM #   Youtube-dl Repository: https://github.com/ytdl-org/youtube-dl  #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #    You can get the newest Windows binary of 'youtube-dl' from    #
REM #    the following URL:                                            #
REM #                                                                  #
REM #             https://yt-dl.org/latest/youtube-dl.exe              #
REM #                                                                  #
REM ####################################################################
REM #                                                                  #
REM #  C H A N G E S :                                                 #
REM #                                                                  #
REM #  Version 1.0                                                     #
REM #                                                                  #
REM #  - Initial version of YouGui for downloading Youtube videos.     #
REM #                                                                  #
REM ####################################################################

REM ####################################################################
REM #     C O N F I G U R A T I O N   S E C T I O N   -   B E G I N    #
REM ####################################################################

REM Specify the Youtube URL.
SET youtube_url=https://www.youtube.com/watch?v=

REM ####################################################################
REM #     C O N F I G U R A T I O N   S E C T I O N   -   E N D        #
REM ####################################################################

REM Enable Delayed Expansion for using if/ELSE constructs with variables.
SETLOCAL EnableDelayedExpansion

REM Begin of the pseudo loop.
:begin

REM Clear the screen.
CLS

REM Display the title.
ECHO ###################################################################
ECHO #                                                                 #
ECHO #   YOUGUI - YOUTUBE-VIDEO-DOWNLOADER-GUI (GUI FOR YOUTUBE-DL)    #
ECHO #                                                                 #
ECHO ###################################################################
ECHO.

REM Ask for a Youtube video code.
ECHO Please enter the Youtube video code from the Youtube URL.
ECHO.
ECHO - ONLY enter the video code and NOT the whole URL^^!
ECHO - As an example the code could look like: p5KgjeZB8Ww
ECHO - You can paste the Youtube video code below :-)
ECHO - Enter 'x' to exit this downloader tool.
ECHO.
SET /P youtube_code=Youtube video code: 

REM Check if the downloader tool should be terminated.
IF "%youtube_code%"=="x" (

    REM Terminate the downloader tool.
    GOTO eof

REM Check if no code was entered.
) ELSE IF "%youtube_code%"=="" (

    REM Display an error message.
    ECHO.
    ECHO No Youtube video code was specified!

    REM Wait before restarting the pseudo loop.
    ECHO.
    PAUSE

    REM Go to the begin of the pseudo loop.
    GOTO begin
)

REM Clear the screen.
CLS

REM Display the whole Youtube URL with the video code.
ECHO Get video/audio formats for the URL: %youtube_url%%youtube_code%...
ECHO.

REM Get the different download formats for the specified video.
youtube-dl.exe -F %youtube_url%%youtube_code%

REM Ask for a video/audio format.
ECHO.
ECHO Please enter a video/audio format:
ECHO.
ECHO - Enter a number listed above to select a specific video/audio format.
ECHO - Enter 'b' to download the video in the best available quality.
ECHO - Enter 'x' to exit this downloader tool.
ECHO.
SET /P youtube_format=Your choice: 

REM Clear the screen.
CLS

REM Check if the downloader tool should be terminated.
IF "%youtube_format%"=="x" (

    REM Terminate the downloader tool.
    GOTO eof

REM Check if no video/audio format was entered.
) ELSE IF "%youtube_format%"=="" (

    REM Display an error message.
    ECHO.
    ECHO No video/audio format was specified!

    REM Wait before restarting the pseudo loop.
    ECHO.
    PAUSE

    REM Go to the begin of the pseudo loop.
    GOTO begin

REM A video/audio format was entered.
) ELSE (

    REM Check if the format should be the best available video format.
    IF "%youtube_format%"=="b" (

        REM Display the selected video/audio format.
        ECHO Selected format: Best available video format

        REM Specify the video/audio format.
        SET youtube_format=best

    REM The format is not 'b' for the best one.
    ) ELSE (

        REM Display the selected video/audio format.
        ECHO Selected format: %youtube_format%

        REM Specify the video/audio format.
        SET youtube_format=%youtube_format%
    )

    REM Display some information.
    ECHO.
    ECHO Starting the download with 'youtube-dl'...

    REM Start downloading the video.
    youtube-dl.exe -f !youtube_format! %youtube_url%%youtube_code%
)

REM Pause the script.
ECHO.
PAUSE

REM GOTO the begin of the pseudo loop.
GOTO begin

REM Marker for jumping to the end of the file.
:eof
